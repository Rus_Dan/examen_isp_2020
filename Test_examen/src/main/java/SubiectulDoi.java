import javax.swing.*;

class MainView {
    public static void main(String args[]){
        var frame = new JFrame("Subiectul Doi");
        var button = new JButton("Press");
        var textFieldOne = new JTextField(50);
        var textFieldTwo = new JTextField(50);

        var mainFrame = new JPanel();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(0,0,200,200);

        button.addActionListener(e -> {
            var text = textFieldOne.getText();
            textFieldTwo.setText(String.valueOf(text.length()));
            textFieldOne.setText("");
        });

        mainFrame.add(textFieldOne);
        mainFrame.add(textFieldTwo);
        mainFrame.add(button);

        frame.getContentPane().add(mainFrame);
        frame.pack();
        frame.setVisible(true);
    }
}
