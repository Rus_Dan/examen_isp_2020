class C{
}

class E extends C{

    private F f;

    public void metG(int i){}
}

class F{
    public void metA(){}
}

class B{
    private long t;

    public D d;

    public B(){
        var e = new E();
    }

    public void f(){}
    public void metJ(A j){}
}

class A{
}

class D{
}
